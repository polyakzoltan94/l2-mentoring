ps:
	docker-compose ps

build:
	docker-compose up -d --build

up:
	docker-compose up -d

down:
	docker-compose down

shell:
	docker exec -it mvc-web bash

codesniffer:
	 ./vendor/squizlabs/php_codesniffer/bin/phpcs app/

phpmd:
	./vendor/phpmd/phpmd/src/bin/phpmd app/ json codesize,unusedcode,naming

phpstan:
	./vendor/bin/phpstan analyse app/

unit:
	docker-compose run -w /var/www/html/tests web /var/www/html/vendor/phpunit/phpunit/phpunit --no-configuration /var/www/html/tests
