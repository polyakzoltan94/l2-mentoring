# L2 mentoring

## Install
Run the following commands in the project's root folder:

- docker-compose up -d --build
- composer install

### Registering new routes

- You can find the public/index.php file
- In this file you can register new routes with the following format:

```php
$app->router->get('<path>', [<exact_controller_namespace_with_class>, '<name_of_the_function>']);
```

Example:
```php
$app->router->get('/foo', [Controller\Bar\Foo::class, 'foo']);
```

You can also create POST request endpoints by using the **Router::post()** call!

## Static code analyzing

- You can find three code analyzer tool in the project currently.

(Prerequisites) In order to use them you have to:

- You have to install the make tool: sudo apt install make
#### OR
- You can run directly these commands within the make file

You can find the Makefile in the project root!

Examples: 

```bash
codesniffer:
	 ./vendor/squizlabs/php_codesniffer/bin/phpcs app/

phpmd:
	./vendor/phpmd/phpmd/src/bin/phpmd app/ json codesize,unusedcode,naming

phpstan:
	./vendor/bin/phpstan analyse app/
```

*phpmd* will return json, (*codesniffer, phpstan*) while they post their notes on the terminal UI

## Unit tests

In order to run the unit tests, you need to run he following command:

- make unit
