<?php
namespace tests\app;

use PHPStan\Testing\TestCase;
use ReflectionClass;

/**
 * Class AbstractTest
 * @package tests\app
 */
abstract class AbstractTest extends TestCase
{
    /**
     * @param $object
     * @param $property
     * @return mixed
     * @throws \ReflectionException
     */
    public function setPropertyToVisibile($object, $property)
    {
        $reflection = new ReflectionClass($object);
        $property = $reflection->getProperty($property);
        $property->setAccessible(true);
        return $property->getValue($this->router);
    }
}
