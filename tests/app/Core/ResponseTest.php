<?php
namespace tests\app\Core;

use InvalidArgumentException;
use App\Core\Response;
use \PHPUnit\Framework\TestCase;

/**
 * Class ResponseTest
 * @package tests\app\Core
 */
class ResponseTest extends TestCase
{
    /**
     * @var Response
     */
    public Response $response;

    /**
     *
     */
    public function setUp(): void
    {
        $this->response = new Response();
    }

    /**
     *
     */
    public function testWithStatusValid()
    {
        $this->assertEquals(
            new Response(200, [], null, 'something'),
            $this->response->withStatus(200, 'something')
        );
    }

    /**
     *
     */
    public function testWithStatusInValid()
    {
        $this->expectException(InvalidArgumentException::class);

        $this->response->withStatus(201, 'something');
    }
}
