<?php
namespace tests\app\Core;

use App\Core\Application;
use App\Core\Request;
use App\Core\Response;
use App\Core\Router;
use \PHPUnit\Framework\TestCase;

/**
 * Class ApplicationTest
 * @package tests\app\Core
 */
class ApplicationTest extends TestCase
{
    /**
     * @var Application
     */
    public Application $application;

    public function setUp(): void
    {
        $this->application = new Application(
            $router = new Router(
                new Request(
                    'get',
                    '/'
                ),
                new Response()
            )
        );
    }

    /**
     * @doesNotPerformAssertions
     */
    public function testRun()
    {
        /**
         * TODO: I'm not quite sure how to test this one.
         * I've tried to check the output buffer.
         */

//        $this->application->run();
    }
}
