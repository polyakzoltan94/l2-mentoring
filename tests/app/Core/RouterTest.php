<?php
namespace tests\app\Core;

use App\Core\Request;
use App\Core\Response;
use App\Core\Router;
use tests\app\Core\Router as SampleRouter;
use tests\app\AbstractTest;

/**
 * Class RouterTest
 * @package tests\app\Core
 */
class RouterTest extends AbstractTest
{
    /**
     * @var Router
     */
    public Router $router;

    /**
     * @var Request
     */
    public Request $request;

    /**
     * @var Response
     */
    public Response $response;

    /**
     *
     */
    protected function setUp(): void
    {
        $this->response = new Response();
        $this->request = new Request('get', '/');
        $this->router = new Router($this->request, $this->response);
    }

    /**
     *
     */
    public function testResolve()
    {
        $this->router->get('/', [SampleRouter\SampleController::class, 'index']);
        $this->router->get('/user', [SampleRouter\SampleController2::class, 'login']);
        $this->router->post('/user', [SampleRouter\SampleController2::class, 'login']);

        $this->assertEquals(
            'index',
            $this->router->resolve()
        );
    }
}
