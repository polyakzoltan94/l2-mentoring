<?php
namespace tests\app\Core;

use App\Core\Request;
use \PHPUnit\Framework\TestCase;

/**
 * Class RequestTest
 * @package tests\app\Core
 */
class RequestTest extends TestCase
{
    /**
     * @var Request
     */
    public Request $request;

    /**
     *
     */
    public function setUp(): void
    {
        $this->request = new Request('GET', '/something?cat=Siam');
    }

    /**
     *
     */
    public function testGetRequestTargetNotEmpty()
    {
        $this->request->withRequestTarget('index/user');

        $this->assertEquals(
            'index/user',
            $this->request->getRequestTarget()
        );
    }

    /**
     *
     */
    public function testGetRequestTargetDynamic()
    {
        $_SERVER['REQUEST_URI'] = '/something?cat=Siam';

        $this->assertEquals(
            '/something?cat=Siam',
            $this->request->getRequestTarget()
        );
    }

    /**
     *
     */
    public function testGetPath()
    {
        $_SERVER['REQUEST_URI'] = '/someurl/with?query=param';

        $this->assertEquals(
            '/someurl/with',
            $this->request->getPath()
        );
    }

    /**
     *
     */
    public function testGetQuery()
    {
        $_SERVER['REQUEST_URI'] = '/someurl/with?query=param';

        $this->assertEquals(
            'query=param',
            $this->request->getQuery()
        );
    }
}
