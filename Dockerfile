FROM php:7.4-apache

RUN a2enmod rewrite && service apache2 restart
RUN pecl install xdebug

COPY ./config/99-xdebug.ini /usr/local/etc/php/conf.d/
