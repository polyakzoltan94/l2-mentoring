<?php
require_once "../vendor/autoload.php";
require_once "../app/Core/functions.php";

use App\Controllers;
use App\Core\Application;
use App\Core\Request;
use App\Core\Response;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

$containerBuilder = new ContainerBuilder();
$containerBuilder->register('application', '\App\Core\Application')
    ->addArgument(new Reference('router'));
$containerBuilder->register('router', '\App\Core\Router')
    ->addArgument(new Request($_SERVER['REQUEST_METHOD'], $_SERVER['REQUEST_URI'], [], prepareRequestBody()))
    ->addArgument(new Response());

try {
    /** @var Application $app */
    $app = $containerBuilder->get('application');
} catch (Exception $e) {
    echo $e->getMessage();
    exit;
}

$app->router->get('/', [Controllers\Main\MainController::class, 'index']);
$app->router->get('/user/login', [Controllers\Auth\AuthController::class, 'login']);
$app->router->post('/user/login', [Controllers\Auth\AuthController::class, 'login']);
$app->router->get('/user/register', [Controllers\Auth\AuthController::class, 'register']);
$app->router->get('/user/logout', [Controllers\Auth\AuthController::class, 'logout']);
$app->router->get('/users', [Controllers\Users\UserController::class, 'listing'], ['auth']);
$app->router->get('/items', [Controllers\Item\ItemController::class, 'listing']);
$app->router->post('/user/register', [Controllers\Auth\AuthController::class, 'register']);
$app->run();
