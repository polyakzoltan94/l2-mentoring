<?php
namespace App\Controllers\Item;

use App\Core\AbstractController;
use App\Core\Request;
use App\Core\Response;

/**
 * Class ItemController
 * @package App\Controller\Item
 */
class ItemController extends AbstractController {
    /**
     * @param Request $request
     * @param Response $response
     * @return Response
     */
    public function listing(Request $request, Response $response): Response
    {
        if ($_SESSION['userType'] != 'admin') {
            $response->withBody('You do not have the right to view this page!');
        }

        return $response;
    }
}
