<?php
namespace App\Controllers\Main;

use App\Core\AbstractController;
use App\Core\Request;
use App\Core\Response;

/**
 * Class AuthController
 * @package App\Controller\Auth
 */
class MainController extends AbstractController
{
    /**
     * @param Request $request
     * @param Response $response
     * @return Response
     */
    public function index(Request $request, Response $response): Response
    {
        $response->withBody('MainController::index()');
        return $response;
    }
}
