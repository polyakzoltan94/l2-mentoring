<?php
namespace App\Controllers\Auth;

use App\Core\AbstractController;
use App\Core\Request;
use App\Core\Response;

/**
 * Class AuthController
 * @package App\Controller\Auth
 */
class AuthController extends AbstractController
{
    /**
     * @param Request $request
     * @param Response $response
     * @return Response
     */
    public function login(Request $request, Response $response): Response
    {
        if ($request->isPost()) {
            $post = $request->getDeserializedBody();
            $users = json_decode(
                file_get_contents(
                    dirname(__DIR__) . '/../../app/Models/Users/users.json'
                ), true
            );

            foreach ($users as $key => $user) {
                if(
                    $user['email'] === $post['email']
                    && password_verify($post['password'], $user['password'])
                ) {
                    session_start();
                    $_SESSION['logged_in'] = true;
                    $_SESSION['email'] = $user['email'];
                    $_SESSION['name'] = $user['name'];
                    $_SESSION['userType'] = $user['userType'];
                }
            }
        }

        include dirname(__DIR__) . '/../Views/Auth/login.php';
        return $response;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response
     */
    public function logout(Request $request, Response $response): Response
    {
        if ($_SESSION['logged_in']) {
            session_destroy();
            unset($_SESSION);
        }
        header('Location: /user/login');
        return $response;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response
     */
    public function register(Request $request, Response $response): Response
    {
        $response->withBody('AuthController::register()');
        return $response;
    }
}
