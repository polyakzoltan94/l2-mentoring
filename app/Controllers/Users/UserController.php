<?php
namespace App\Controllers\Users;

use App\Core\AbstractController;
use App\Core\Request;
use App\Core\Response;

/**
 * Class AuthController
 * @package App\Controller\Auth
 */
class UserController extends AbstractController
{
    /**
     * @param Request $request
     * @param Response $response
     * @return Response
     */
    public function listing(Request $request, Response $response): Response
    {
        $response->withBody('UserController::listing()');
        return $response;
    }
}
