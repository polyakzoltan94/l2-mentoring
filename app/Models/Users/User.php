<?php
namespace App\Models\Users;

use App\Core\AbstractModel;

/**
 * Class User
 * @package App\Models\Users
 */
class User extends AbstractModel {
    /**
     * @var string
     */
    const USER_ADMIN = 'admin';

    /**
     * @var string
     */
    const USER_GENERAL = 'general';

    /**
     * @var int
     */
    protected int $userId;

    /**
     * @var string
     */
    protected string $email;

    /**
     * @var string
     */
    protected string $password;

    /**
     * @var string
     */
    protected string $userType;

    /**
     * @var array
     */
    protected static array $userTypes = [
        self::USER_GENERAL,
        self::USER_ADMIN,
    ];
}
