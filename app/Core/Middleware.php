<?php
namespace App\Core;

/**
 * Class Middleware
 * @package App\Core
 */
class Middleware
{
    /**
     * @param array
     */
    const GUARDS = [
        'auth'
    ];

    /**
     * @var array
     */
    public array $guards;

    /**
     * Middleware constructor.
     * @param array $guard
     */
    public function __construct(array $guard)
    {
        $this->guards = $guard;
    }

    /**
     * @return bool
     */
    public function isGuardsExist(): bool
    {
        foreach ($this->guards as $guard) {
            if (!in_array($guard, self::GUARDS)) {
                return false;
            }
        }

        return true;
    }

    /**
     * @return bool
     */
    public function auth(): bool
    {
        return $_SESSION['logged_in'] ?? false;
    }
}