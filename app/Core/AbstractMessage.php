<?php
namespace App\Core;

use Psr\Http\Message\MessageInterface;

/**
 * Class AbstractMessage
 * @package App\Core
 */
abstract class AbstractMessage implements MessageInterface
{
    /**
     * @var array
     */
    protected array $headers = [];

    /**
     * @var array
     */
    protected array $headerNames = [];

    /**
     * @var string
     */
    protected string $protocol = '1.1';

    /**
     * @var string|null
     */
    protected ?string $body;

    /**
     * @return string
     */
    public function getProtocolVersion(): string
    {
        return $this->protocol;
    }

    /**
     * @param string $version
     * @return $this
     */
    public function withProtocolVersion($version): MessageInterface
    {
        if ($this->protocol === $version) {
            return $this;
        }

        $this->protocol = $version;

        return $this;
    }

    /**
     * @return array
     */
    public function getHeaders(): array
    {
        return $this->headers;
    }

    /**
     * @param string $header
     * @return bool
     */
    public function hasHeader($header): bool
    {
        return isset($this->headerNames[strtolower($header)]);
    }

    /**
     * @param string $header
     * @return array
     */
    public function getHeader($header): array
    {
        $header = strtolower($header);

        if (!isset($this->headerNames[$header])) {
            return [];
        }

        $header = $this->headerNames[$header];

        return $this->headers[$header];
    }

    /**
     * @param string $header
     * @return string
     */
    public function getHeaderLine($header)
    {
        return implode(', ', $this->getHeader($header));
    }

    /**
     * @param string $header
     * @param string|string[] $value
     * @return $this
     */
    public function withHeader($header, $value): MessageInterface
    {
        $header = strtolower($header);

        if (isset($this->headerNames[$header])) {
            unset($this->headers[$this->headerNames[$header]]);
        }

        $this->headerNames[$header] = $header;
        $this->headers[$header] = $value;

        return $this;
    }

    /**
     * @param string $header
     * @param string|string[] $value
     * @return $this
     */
    public function withAddedHeader($header, $value): MessageInterface
    {
        $header = strtolower($header);

        if (isset($this->headerNames[$header])) {
            $header = $this->headerNames[$header];
            $this->headers[$header] = array_merge($this->headers[$header], $value);
        } else {
            $this->headerNames[$header] = $header;
            $this->headers[$header] = $value;
        }

        return $this;
    }

    /**
     * @param string $header
     * @return $this
     */
    public function withoutHeader($header): MessageInterface
    {
        $header = strtolower($header);

        if (!isset($this->headerNames[$header])) {
            return $this;
        }

        $header = $this->headerNames[$header];
        unset($this->headers[$header], $this->headerNames[$header]);

        return $this;
    }

    /**
     * @return string|null
     */
    public function getBody(): ?string
    {
        return $this->body;
    }

    /**
     * @param string $body
     * @return $this
     */
    public function withBody($body): MessageInterface
    {
        if ($body === $this->body) {
            return $this;
        }

        $this->body = $body;

        return $this;
    }
}
