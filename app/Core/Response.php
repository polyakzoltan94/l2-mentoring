<?php
namespace App\Core;

use Psr\Http\Message\ResponseInterface;

/**
 * Class Response
 * @package App\Core
 */
class Response extends AbstractMessage implements ResponseInterface
{
    /**
     * @var int
     */
    protected int $statusCode;

    /**
     * @var string
     */
    protected string $reasonPhrase;

    /**
     * @var array
     */
    private const STATUS_CODES = [
        200 => 'OK',
        400 => 'Bad Request',
        401 => 'Unauthorized',
        402 => 'Payment Required',
        403 => 'Forbidden',
        404 => 'Not Found',
        500 => 'Internal Server Error',
    ];

    /**
     * Response constructor.
     * @param int $status
     * @param array $headers
     * @param string|null $body
     * @param string|null $reason
     */
    public function __construct(
        int $status = 200,
        array $headers = [],
        ?string $body = null,
        string $reason = null
    ) {
        $this->statusCode = $status;
        $this->body = $body;

        foreach ($headers as $header => $value) {
            $this->withHeader($header, $value);
        }

        if ($reason == '' && isset(self::STATUS_CODES[$this->statusCode])) {
            $this->reasonPhrase = self::STATUS_CODES[$this->statusCode];
        } else {
            $this->reasonPhrase = (string) $reason;
        }
    }

    /**
     * @return int
     */
    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    /**
     * @param int $code
     * @param string $reasonPhrase
     * @return $this
     */
    public function withStatus($code, $reasonPhrase = ''): ResponseInterface
    {
        $code = (int) $code;

        if (!isset(self::STATUS_CODES[$code])){
            throw new \InvalidArgumentException('Invalid status code.');
        }

        $this->statusCode = $code;
        $this->reasonPhrase = (string) $reasonPhrase;

        return $this;
    }

    /**
     * @return string
     */
    public function getReasonPhrase(): string
    {
        return $this->reasonPhrase;
    }
}
