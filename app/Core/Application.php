<?php 
namespace App\Core;

/**
 * Class Application
 * @package App\Core
 */
class Application
{
    /**
     * @var Application
     */
    public static Application $app;

    /**
     * @var Router
     */
    public Router $router;

    /**
     * @param Router $router
     */
    public function __construct(Router $router)
    {
        self::$app = $this;
        $this->router = $router;
    }

    /**
     *
     */
    public function run()
    {
        echo $this->router->resolve()->getBody() ?? '';
    }
}
