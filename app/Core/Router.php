<?php
namespace App\Core;

/**
 * Class Router
 * @package App\Core
 */
class Router
{
    /**
     * @var Request
     */
    public Request $request;

    /**
     * @var Response
     */
    public Response $response;

    /**
     * @var array
     */
    protected array $routes = [];

    /**
     * Router constructor.
     * @param Request $request
     * @param Response $response
     */
    public function __construct(Request $request, Response $response)
    {
        $this->request = $request;
        $this->response = $response;
    }

    /**
     * @param string $path
     * @param array $callback
     * @param array|null $middleware
     * @return self
     */
    public function get(string $path, array $callback, ?array $middleware = null): Router
    {
        $this->routes['GET'][$path] = [
            'controller' => $callback[0],
            'function' => $callback[1],
            'middleware' => $middleware,
        ];

        return $this;
    }

    /**
     * @param string $path
     * @param array $callback
     * @param array|null $middleware
     * @return self
     */
    public function post(string $path, array $callback, ?array $middleware = null): Router
    {
        $this->routes['POST'][$path] = [
            'controller' => $callback[0],
            'function' => $callback[1],
            'middleware' => $middleware,
        ];
        return $this;
    }

    /**
     * @return mixed
     */
    public function resolve()
    {
        $method = $this->request->getMethod();
        $path = $this->request->getPath(); 
        $callback = $this->routes[$method][$path];

        if (is_null($callback)) {
            $this->response->withStatus(404);
            http_response_code($this->response->getStatusCode());
            exit;
        }

        if (
            $callback['middleware']
            && !$this->checkMiddleware($callback)
        ) {
            $this->response->withStatus(401);
            http_response_code($this->response->getStatusCode());
            exit;
        }

        unset($callback['middleware']);

        $callback = array_values($callback);

        if (is_array($callback)) {
            $callback[0] = new $callback[0];
        }

        return call_user_func($callback, $this->request, $this->response);
    }

    /**
     * @param array $endpoint
     * @return bool
     */
    public function checkMiddleware(array $endpoint): bool
    {
        $middleware = new Middleware($endpoint['middleware']);

        if (!$middleware->isGuardsExist()) {
            $this->response->withStatus(500);
            http_response_code($this->response->getStatusCode());
            exit;
        }

        return call_user_func([$middleware, current($endpoint['middleware'])]);
    }
}
