<?php

/**
 * @return string
 */
function prepareRequestBody(): string
{
    $result = [];

    switch ($_SERVER['REQUEST_METHOD']) {
        case 'POST':
            foreach($_POST as $key => $data) {
                $result[$key] = filter_input(INPUT_POST, $key, FILTER_SANITIZE_SPECIAL_CHARS);
            }
            break;
        case 'GET':
            foreach($_GET as $key => $data) {
                $result[$key] = filter_input(INPUT_GET, $key, FILTER_SANITIZE_SPECIAL_CHARS);
            }
            break;
    }

    return serialize($result);
}
