<?php
namespace App\Core;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\UriInterface;

/**
 * Class Request
 * @package App\Core
 */
class Request extends AbstractMessage implements RequestInterface
{
    /**
     * @var string
     */
    protected string $method;

    /**
     * @var string|null
     */
    protected ?string $requestTarget;

    /**
     * @var string
     */
    protected string $uri;

    /**
     * Request constructor.
     * @param string $method
     * @param string $uri
     * @param array $headers
     * @param string|null $body
     */
    public function __construct(
        string $method,
        string $uri,
        array $headers = [],
        ?string $body = null
    ) {
        $this->method = strtoupper($method);
        $this->uri = $uri;
        $this->body = $body;

        foreach ($headers as $header => $value) {
            $this->withHeader($header, $value);
        }
    }

    /**
     * @return string|null
     */
    public function getRequestTarget(): ?string
    {
        if (isset($this->requestTarget) && $this->requestTarget !== null) {
            return $this->requestTarget;
        }

        $target = $this->getPath();

        if ($target === '') {
            $target = '/';
        }

        if ($this->getQuery() != '') {
            $target .= '?' . $this->getQuery();
        }

        return $target;
    }

    /**
     * @param string|null $requestTarget
     * @return RequestInterface
     */
    public function withRequestTarget($requestTarget): RequestInterface
    {
        $this->requestTarget = $requestTarget;
        return $this;
    }

    /**
     * @return string
     */
    public function getMethod(): string
    {
        return $this->method;
    }

    /**
     * @param string $method
     * @return RequestInterface
     */
    public function withMethod($method): RequestInterface
    {
        $this->method = strtoupper($method);
        return $this;
    }

    /**
     * @return string
     */
    public function getUri(): string
    {
        return $this->uri;
    }

    public function withUri(UriInterface $uri, $preserveHost = false)
    {
        // TODO: Implement withUri() method.
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        $path = $_SERVER['REQUEST_URI'] ?? '/';
        $position = strpos($path, '?');

        if (!$position && is_bool($position)) {
            return $path;
        }

        return substr($path, 0, $position);
    }

    /**
     * @return string
     */
    public function getQuery(): string
    {
        $path = $_SERVER['REQUEST_URI'];
        $position = strpos($path, '?');

        if (!$position) {
            return '';
        }

        return substr($path, $position + 1, strlen($path));
    }

    /**
     * @return bool
     */
    public function isPost(): bool
    {
        return $this->getMethod() === 'POST';
    }

    /**
     * @return array
     */
    public function getDeserializedBody(): array
    {
        return unserialize($this->getBody());
    }
}
