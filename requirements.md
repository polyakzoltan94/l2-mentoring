###Custom Framework #1 – Basics

Create a minimal project setup that will serve as the base for your custom framework implementation. In this first step you should create a minimal routing and get all the tooling set up that will be required.

Requirements are the following:

· Build your setup based on docker. Usage of docker compose is strongly suggested.

· Use git for version control.

· Use composer for autoloading and dependency management.

· Use a PSR-11 compatible service container for your application. Example: https://symfony.com/doc/current/components/dependency_injection.html

· Create a front controller that handles all the request response lifecycle. It should conform to and use PSR-7. High level concept: (Request -> Controller -> Response) function controllerAction(Request $request): Response

· Create a basic routing system. The minimum things that it should cover are:

    - It should be possible to register routes in a static way. It can be a simple array with “URI => route options” mappings.

    - It should be possible to handle routes with same URI but with different HTTP methods.

    - Create 3 different example routes with controllers. Don’t overcomplicate the response and the controllers. Simple short strings as responses are more than enough, templating and other things are not yet needed.

· Integrate at least 3 static code analyzers into your HW.

· Please create a README.MD file in your project root and document the following in it:

o How can someone create and register a new route in your framework.

o How can you run each of the static code analyzer and what is the output of each of them.

___

###Custom Framework #2 – Testing

Please add unit testing to your existing code. From now on please add appropriate tests to all of your future homework.

Requirements:

· Use PHPUnit to create your unit tests

· Add details to your README.MD file about how to run your tests

OPTIONAL:

· Add a few integration tests to your framework

· Add Behat tests to your framework

___

###Custom Framework #3 – Security

Add basic but must have security measures to your existing code. E.g. hide PHP, prohibit remote execution, protect it against SQL injection, XSS and CSRF. From now on please consider every security concerns that you have just learned.

Add Authentication to your application.

Requirements:

Implement basic user authentication to your application, protect your pages against users that are not logged in properly.

Implement basic authorization, create at least 2 user types and give them different kind of controls. 

```
    PHP.ini secusítás
    tipikus támadás ellen felkészíteni, csrf, xss, sqlinj
    page védelem, access nélkül
    user definíciók access control
```

